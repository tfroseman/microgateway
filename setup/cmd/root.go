package cmd

import (
	"flag"
	"fmt"
	"github.com/spf13/cobra"
	"k8s.io/klog"
	"microgateway/util"
	"os"
)

var rootCmd = &cobra.Command{
	Use:     "edgemicro_setup",
	Short:   "Microgateway is a small locally host proxy for your Apigee API endpoints",
	Long:    `Apigee Edgemicrogateway setup utility. Use this to manage tokens, auth, and setup configs`,
	Example: "",
	Args:    cobra.MinimumNArgs(1),
}

var (
	Verbose bool
)

func init() {
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output at all levels")
	klog.InitFlags(nil)
	err := flag.Set("log_file", "microgateway.log")

	util.CheckErr(err)

	flag.Parse()
	klog.Info("Starting microgateway")
	klog.Flush()
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
