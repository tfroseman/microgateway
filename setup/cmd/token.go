package cmd

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/cobra"
	"log"
	"microgateway/util"
)

var (
	organization   string
	environment    string
	consumerKey    string
	consumerSecret string
	privateCloud   bool
	edgemicro_auth string
	tokenFile      string
	token          string

	tokenCmd = &cobra.Command{
		Use:   "token",
		Short: "Manage JWTs",
		Long:  "Manage JWTs",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("Do stuff with tokens")
		},
	}

	decodeTokenCMD = &cobra.Command{
		Use:   "decode",
		Short: "Decode a JWT",
		Long:  "Decode a provided token via by file or parameter",
		Run:   decodeToken,
	}

	encodeTokenCMD = &cobra.Command{
		Use:   "encode",
		Short: "encode values into a JWT",
		Long:  "Encode a file or given paramters into a JWT",
		Run:   encodeToken,
	}

	getTokenCMD = &cobra.Command{
		Use:   "get",
		Short: "create a client_credentials jwt",
		Long:  "create a client_credentials jwt",
		Run:   getToken,
	}

	verifyTokenCMD = &cobra.Command{
		Use:   "verify",
		Short: "verify a JWT against the public key",
		Long:  "verify a JWT against the public key",
		Run:   verifyToken,
	}
)

func init() {
	rootCmd.AddCommand(tokenCmd)
	tokenCmd.AddCommand(decodeTokenCMD, encodeTokenCMD, getTokenCMD, verifyTokenCMD)

	getTokenCMD.Flags().StringVarP(&organization, "org", "o", "", "Organization")
	getTokenCMD.MarkFlagRequired("org")

	getTokenCMD.Flags().StringVarP(&environment, "env", "e", "", "Environment")
	getTokenCMD.MarkFlagRequired("env")

	getTokenCMD.Flags().StringVarP(&consumerKey, "key", "k", "", "Consumer Key")
	getTokenCMD.MarkFlagRequired("key")

	getTokenCMD.Flags().StringVarP(&consumerSecret, "secret", "s", "", "Consumer Secret")
	getTokenCMD.MarkFlagRequired("secret")



	decodeTokenCMD.Flags().StringVarP(&tokenFile, "file", "f", "", "Path to file containing JWT")
	decodeTokenCMD.Flags().StringVarP(&token, "token", "t", "", "JWT")
	decodeTokenCMD.Flags().StringVarP(&organization, "org", "o", "", "Organization")
	decodeTokenCMD.MarkFlagRequired("org")

	decodeTokenCMD.Flags().StringVarP(&environment, "env", "e", "", "Environment")
	decodeTokenCMD.MarkFlagRequired("env")
}

/**
Decode a given token. This can be a file containg a jwt or via an argument
*/
func decodeToken(cmd *cobra.Command, args []string) {

	var (
		jwtString string
		publickey string
	)

	edgemicroConfig := util.LoadConfig(organization, environment)

	if tokenFile == "" && token == "" {
		log.Fatalf("A token or file path must be supplied\n")
	}

	if token == "" {
		jwtString = util.LoadToken(tokenFile)
	} else {
		jwtString = token
	}

	parsedJWT, err := jwt.Parse(jwtString, func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected siging method: %v", token.Header["alg"])
		}

		util.NewRequest("GET", edgemicroConfig.Jwt_public_key, make([]byte, 0))
		publickey = util.ExecuteRequest()

		rsaPublickey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(publickey))

		if err != nil {
			log.Fatal(err)
		}

		return rsaPublickey, nil
	})

	if claims, ok := parsedJWT.Claims.(jwt.MapClaims); ok && parsedJWT.Valid {
		fmt.Println(claims)
	} else {
		fmt.Println(err)
	}
}

/**

 */
func verifyToken(cmd *cobra.Command, args []string) {
	// Get saved token from file
	//jwtString := readTokenFromFile(options.TokenFile)

	//fmt.Println(jwtString)

	//const targetPath = configLocations.getSourcePath(options.org, options.env);
	//const config = edgeconfig.load({ source: targetPath, keys: keys });

	//const authUri = config.edge_config['authUri'];
	//this.isPublicCloud = config.edge_config['managementUri'] === 'https://api.enterprise.apigee.com' || config.edge_config['managementUri'] === 'https://api.e2e.apigee.net';

	//getPublicKey(options.org, options.env, authUri, this.isPublicCloud, function(err, certificate) {

	//const opts = {
	//algorithms: ['RS256'],
	//ignoreExpiration: false
	//};

	//jwt.verify(token, certificate, opts, function(err, result) {
	//if (err) {
	//cb(err)
	//return printError(err);
	//}
	//console.log(result);
	//cb(null,result)
	//});
	//return errors.New("Config options: TokenFile, Org, Env, Key or Secret can not be empty or null")
}

/**
Obtain a new JWT(json web token). This command requires: org, env, consumer key, consumer secret.
*/
func getToken(cmd *cobra.Command, args []string) {
	tokenPayload := `{"client_id":"%s", "client_secret":"%s", "grant_type":"client_credentials"}`

	/**
	Confirm that all values needed are set
	*/
	if organization == "" || environment == "" || consumerKey == "" || consumerSecret == "" {
		log.Fatal("Organization, Environment, Consumer Key, and Consumer Secret can not be null")
	}

	/**
	Load default microgateway edgemicroConfig
	*/
	edgemicroConfig := util.LoadConfig(organization, environment)

	/**
	construct the url to get the token
	https://troseman-eval-test.apigee.net/edgemicro-auth/token
	*/
	edgemicro_auth := fmt.Sprintf(edgemicroConfig.AuthUri, organization, environment)

	/**
	Send key and secret to obtain a new JWT
	*/
	payload := []byte(fmt.Sprintf(tokenPayload, consumerKey, consumerSecret))

	/**
	Create and execute a new request
	*/
	util.NewRequest("POST", edgemicro_auth+"/token", payload)
	util.AddHeader(util.HTTPHeader{Key: "Content-Type", Value: "application/json"})
	tokenResponse := util.ExecuteRequest()

	/**
	Print token response to console
	*/
	fmt.Printf("%v", tokenResponse)
}

/**
With given arguments. Encode the json payload
*/
func encodeToken(cmd *cobra.Command, args []string) {

}

/**
Used by verify to pull down the public key from apigee
*/
func getPublicKey(organization string, evinronmet string, authURI string, isPublicCloud string) {
	//function getPublicKey(organization, environment, authUri, isPublicCloud, cb) {
	//
	//const uri = isPublicCloud ? util.format(authUri + '/publicKey', organization, environment) : authUri + '/publicKey';
	//request({
	//uri: uri
	//}, function(err, res) {
	//if (err) { return cb(err); }
	//cb(null, res.body);
	//});
	//}
	//
}
