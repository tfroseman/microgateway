package cmd

import (
	"log"
	"os"
	"os/user"
)

var DEFAULT_DIR = os.Getenv("HOME") + ".microgateway"

func Load() {

}

func getHomeDir() string {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return usr.HomeDir
}
