package main

import (
	"microgateway/setup/cmd"
)

func main() {
	cmd.Execute()
}
