package util

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os/user"
)

const (
	DEFAULT_DIR = ".edgemicro"
)

var (
	edgeMicro     EdgemicroConfig
	IsPublicCloud bool
)

/**
TODO Remove this dep
*/
func CheckErr(err error) {
	log.Fatal(err)
}

/**
Organization string, Environment string
*/
func LoadConfig(organization string, environment string) EdgemicroConfig {

	var configDir = getHomeDir() + "/" + DEFAULT_DIR + "/" + organization + "-" + environment + "-config.yaml"

	configString := string(openFile(configDir))

	if yamlError := yaml.UnmarshalStrict([]byte(configString), &edgeMicro); yamlError != nil {
		log.Fatalf("There was an issue Unmarshalling the yaml config file: %v", yamlError)
	}

	IsPublicCloud = isPublicCloud(edgeMicro.ManagementUri)

	return edgeMicro
}

/**
Load a JWT from a file
*/
func LoadToken(filePath string) string {
	fileContents := openFile(filePath)
	return string(fileContents)
}

/**
Generic wrapper to return a byte array of the file contents
*/
func openFile(filePath string) []byte {
	if len(filePath) == 0 {
		log.Fatal("File path missing")
	}

	contents, err := ioutil.ReadFile(filePath)

	if err != nil {
		log.Fatalf("There was a problem opening : %v", err)
	}

	return contents
}

/**
Get $HOME on unix like systems
*/
func getHomeDir() string {
	usr, err := user.Current()

	if err != nil {
		log.Fatalf("%v", err)
	}

	return usr.HomeDir
}

/**
Check if the management api is public or private cloud
*/
func isPublicCloud(managementURL string) bool {
	if managementURL == "https://api.enterprise.apigee.com" {
		return false
	} else {
		return true
	}
}
