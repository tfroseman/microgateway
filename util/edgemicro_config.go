package util

type EdgemicroConfig struct {
	BaseConfig       `yaml:"edge_config"`
	EdgeMicroGateway `yaml:"edgemicro"`
	Headers          `yaml:"headers"`
	Oauth            `yaml:"oauth"`
	Analytics        `yaml:"Analytics"`
}

type BaseConfig struct {
	Bootstrap        string `yaml:"bootstrap"`
	Jwt_public_key   string `yaml:"jwt_public_key"`
	ManagementUri    string `yaml:"managementUri"`
	VaultName        string `yaml:"vaultName"`
	AuthUri          string `yaml:"authUri"`
	BaseUri          string `yaml:"baseUri"`
	BootstrapMessage string `yaml:"bootstrapMessage"`
	KeySecretMessage string `yaml:"keySecretMessage"`
	Products         string `yaml:"products"`
}

type EdgeMicroGateway struct {
	Port                        int `yaml:"port"`
	Max_connections             int `yaml:"max_connections"`
	Config_change_poll_interval int `yaml:"config_change_poll_interval"`
	Logging                     `yaml:"logging"`
	Plugins                     `yaml:"plugins"`
}

type Logging struct {
	Level              string `yaml:"level"`
	Dir                string `yaml:"dir"`
	Stats_log_interval int    `yaml:"stats_log_interval"`
	Rotate_interval    int    `yaml:"rotate_interval"`
}
type Plugins struct {
	Sequence []string `yaml:"sequence"`
}

type Headers struct {
	XForwardFor   bool `yaml:"x-forwarded-for"`
	XForwardHost  bool `yaml:"x-forwarded-host"`
	XRequestId    bool `yaml:"x-request-id"`
	XResponseTime bool `yaml:"x-response-time"`
	Via           bool `yaml:"via"`
}

type Oauth struct {
	AllowNoAuthorization      bool   `yaml:"allowNoAuthorization"`
	AllowInvalidAuthorization bool   `yaml:"allowInvalidAuthorization"`
	VerifyApiKeyUrl           string `yaml:"verify_api_key_url"`
}

type Analytics struct {
	Uri           string `yaml:"uri"`
	BufferSize    int    `yaml:"buff_size"`
	BatchSize     int    `yaml:"batch_size"`
	FlushInterval int    `yaml:"flush_interval"`
}
