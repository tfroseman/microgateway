package util

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

type HTTPHeader struct {
	Key   string
	Value string
}

var (
	httpClient = &http.Client{}

	request       *http.Request
	requestError  error
	response      *http.Response
	responseError error
	bodyString    string
)

func NewRequest(method string, url string, body []byte) {
	request, requestError = http.NewRequest(method, url, bytes.NewBuffer(body))

	CheckErr(requestError)
}

func ExecuteRequest() string {

	response, responseError = httpClient.Do(request)

	CheckErr(responseError)

	defer response.Body.Close()

	responseBody, responseError := ioutil.ReadAll(response.Body)
	CheckErr(responseError)

	return string(responseBody)

}

/**
Add multiple headers to the request
*/
func AddHeaders(headers []HTTPHeader) {
	for _, header := range headers {
		request.Header.Add(header.Key, header.Value)
	}
}

/**
Add a single header to the request
*/
func AddHeader(header HTTPHeader) {
	request.Header.Add(header.Key, header.Value)
}
