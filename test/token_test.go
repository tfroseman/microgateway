package test

import (
	"edgemicro/setup/cmd/token"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"reflect"
	"testing"
)

func TestTokenDecode(t *testing.T) {
	var token jwt.Token = token.DecodeToken("jwt")
	var createdTokenString = EncodeToken()

	// TODO token encoding and decoding produce different results based on claim order
	//if (!reflect.DeepEqual(token, createdTokenString)) {
	//	t.Error("JWT was incorrect, got: ", createdTokenString, ", wanted: ", token)
	//}
}

func EncodeToken() jwt.Token {
	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub":  "1234567890",
		"name": "John Doe",
		"iat":  1516239022,
	})

	//// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString([]byte("secret"))

	fmt.Println(tokenString)

	if err != nil {
		fmt.Println(err)
	}

	tokenObject, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte("secret"), nil
	})

	return *tokenObject
}
